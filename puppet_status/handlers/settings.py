#Only Cert  Settings
#
#Api Hostname
API_HOST = 'https://puppet.ceb.loc:8081/'

# SSL cert path conf dir
CA_CERT = '/var/lib/puppet/ssl/certs/ca.pem'
CERT = '/var/lib/puppet/ssl/certs/stor-es-arch.ceb.loc.pem'
KEY = '/var/lib/puppet/ssl/private_keys/stor-es-arch.ceb.loc.pem'

HOST_TYPE_TITLES_MAP = {
    'ceb_loc': 'Local',
    'amazon_loc': 'Amazon',
    'loc': 'loc',
    'hypercomments_loc' : 'Hypercommets',
    'sender_loc' : 'Sender',
    'middleware_loc' : 'Middleware',
}
HOST_ROLE_TITLES_MAP = {
    'w': 'Workers',
}