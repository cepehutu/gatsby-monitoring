#encoding: utf-8
import re
from settings import HOST_TYPE_TITLES_MAP, HOST_ROLE_TITLES_MAP


def host_type(host_name):
    return '_'.join(host_name.split('.')[-2:])


def host_group_by_type(_json, attr_name):
    groups = {}
    for item in _json:
        host_name = item[attr_name]
        groups[host_type(host_name)] = True
    return groups.keys()


def host_type_title(host_type):
    return HOST_TYPE_TITLES_MAP.get(host_type,  host_type)


def host_role(host_name):
    _tmp = host_name.split('.')[0].split('-')[0]
    return re.sub('[0-9]+', '', _tmp)


def host_group_by_role(_json, attr_name):
    groups = {}
    for item in _json:
        host_name = item[attr_name]
        groups[host_role(host_name)] = True
    return groups.keys()


def host_role_title(host_type):
    return HOST_ROLE_TITLES_MAP.get(host_type,  host_type)