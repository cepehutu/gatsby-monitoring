# -*- coding: utf-8 -*-
import urllib
import tornado.web
import tornado.ioloop
import redis

from datetime import datetime
from tornado.escape import json_decode
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPClient
from datetime import datetime

from . import settings
from . import helpers

redis = redis.StrictRedis(host='localhost', port=6379, db=0)


class BackupsHandler(tornado.web.RequestHandler):
    template = '../templates/backup_status.html'

    resource = 'v3/facts'

    main_query = {'query': '["=", "name", "backups"]'}


    def create_request(self, resource):
#        print settings.API_HOST + resource+'?'+urllib.urlencode(self.main_query)
        return HTTPRequest(url=settings.API_HOST + resource+'?'+urllib.urlencode(self.main_query),
                           method="GET",
                           ca_certs=settings.CA_CERT, client_cert=settings.CERT, client_key=settings.KEY)

    @tornado.web.asynchronous
    def get(self):
        http_client = AsyncHTTPClient()
        http_client.fetch(self.create_request('v3/facts'),  self.on_response)

    def on_response(self, response):
        if response.error:raise tornado.web.HTTPError(500)
        self.render(self.template, **{'json': json_decode(response.body),
                                                          'get_fail_status': self.get_fail_status,
                                                          'get_success_status': self.get_success_status,
                                                          'helpers': helpers})

    @staticmethod
    def parse_to_datetime(_str, format='%b %d %H:%S: '):
        # Jul 02 02:00:
        try:
            _date = datetime.strptime(_str, format)
            return _date.replace(year=datetime.now().year)
        except Exception, e:
            # print e
            return datetime(1970, 1, 1)

    def parse_results(self, backups_string, backup_name):
        backups_list = backups_string.split(',')
        result_string = ''
        backups_list.reverse()
        for item in backups_list:
            if backup_name in item:
                result_string = item
                break
        return self.parse_to_datetime(result_string.split('<<<<')[0])

    def create_status_request(self, query):
        request = HTTPRequest(url=settings.API_HOST + self.resource+'?'+urllib.urlencode(query),
                             method="GET",
                             ca_certs=settings.CA_CERT, client_cert=settings.CERT, client_key=settings.KEY)
        http_client = HTTPClient()
        try:
            response = http_client.fetch(request)
            json = json_decode(response.body)
        except httpclient.HTTPError as e:
            # HTTPError is raised for non-200 responses; the response
            # can be found in e.response.
            print("Error: " + str(e))
        except Exception as e:
            # Other errors are possible, such as IOError.
            print("Error: " + str(e))
        http_client.close()
        if not json:
            return False
        return json

    def get_fail_status(self, host, backup_name):
        cache_key = 'fail:{0}:{1}'.format(host, backup_name)  
        if redis.get(cache_key):
            return redis.get(cache_key)
        failed_query = {'query': '["and", ["=", "name", "failed_backup"], ["=", "certname", "{0}"]]'.format(host)}
        status_response = self.create_status_request(query=failed_query)
        if not status_response:
            return ''
        result = self.parse_results(status_response[0]['value'], backup_name)
        redis.set(cache_key, result, 900)
        return result

    def get_success_status(self, host, backup_name):
        cache_key = 'success:{0}:{1}'.format(host, backup_name)
        if redis.get(cache_key):
            return redis.get(cache_key)
        success_query = {'query': '["and", ["=", "name", "success_backup"], ["=", "certname", "{0}"]]'.format(host)}
        status_response = self.create_status_request(query=success_query)
        if not status_response:
            return ''
        result = self.parse_results(status_response[0]['value'], backup_name)
        redis.set(cache_key, result, 900)
        return result
