Facter.add('success_backup') do
      if File.file?('/var/log/backupninja.log')
      setcode do
            Facter::Core::Execution.exec('cat /var/log/backupninja.log | grep SUCCESS | cut -f 1,2,4 -d : -s |  uniq -u | tr "\n" ", " ')
        end
      else
        setcode do
      Facter::Core::Execution.exec('echo Jan 01 00:00:')
        end
      end
end

Facter.add('failed_backup') do
      if File.file?('/var/log/backupninja.log')
        setcode do
          Facter::Core::Execution.exec('cat /var/log/backupninja.log | grep FAILED | cut -f 1,2,4 -d : -s |  uniq -u | tr "\n" ", " ')
        end
      else 
    setcode do
     Facter::Core::Execution.exec('echo Jan 01 00:00:')
    end
      end
end
